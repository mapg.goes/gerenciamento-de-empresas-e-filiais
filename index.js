const express = require('express')
const database = require("./banco/connection")


const app = express()
const routes = require('./routes')

app.use(express.json())
app.use(routes)

app.use((error , req, res, next) => {

    res.status(error.status || 500)
    res.json({ erro: error.message })
})

app.listen(3333, () => console.log("Servers running"))

