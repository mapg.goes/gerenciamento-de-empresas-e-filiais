const knex = require('../../banco/connection')

module.exports = {
    async index(req, res, next){ 

        try {

            const { 
                nome_usuario, 
                email, 
                senha, 
                id_empresa } = req.body

            await knex('usuarios').insert({ 
                nome_usuario, 
                email, 
                senha, 
                id_empresa 
            })
            
            return res.status(201).send()
            
        } catch (error) {
            next(error)
        }

    }
}