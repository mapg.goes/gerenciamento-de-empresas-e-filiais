const knex = require('../../banco/connection')

module.exports = {
    async index(req, res, next){

        try {
            const { id_usuario } = req.params

            await knex('usuarios').where({ id_usuario }).del()
            await knex('usuario_alocado').where({ id_usuario }).del()
            
            return res.status(201).send()

        } catch (error) {
            next(error)
        }

    }
}