const knex = require('../../banco/connection')

module.exports = {

    async index(req, res, next) {
       try {
        const results = await knex('empresas')
        
        return res.json(results)
           
       } catch (error) {
           next(error)
       } 
    }

}