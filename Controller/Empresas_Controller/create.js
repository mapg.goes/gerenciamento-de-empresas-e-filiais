const knex = require('../../banco/connection')

module.exports = {
    async index(req, res, next){ 
        try {
            const { nome_empresa } = req.body
            
            await knex('empresas').insert({ nome_empresa })
            
            return res.status(201).send()
            
        } catch (error) {
            next(error)
        }

    }
}