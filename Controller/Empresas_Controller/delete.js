const knex = require('../../banco/connection')

module.exports = {
    async index(req, res, next){

        try {
            const { id_empresa } = req.params

            await knex('empresas').where({ id_empresa }).del()
            await knex('filiais').where({ id_empresa }).del()

            res.status(201).send()

        } catch (error) {
            next(error)
        }

    }
}