const knex = require('../../banco/connection')

module.exports = {
    async index(req, res, next){

        try {
            const { id_filial } = req.params

            await knex('filiais').where({ id_filial }).del()
            await knex('usuario_alocado').where({ id_filial }).del()
           
           return res.status(201).send()

        } catch (error) {
            next(error)
        }

    }
}