const knex = require('../../banco/connection')

module.exports = {
    async index(req, res, next){ 
        
        try {
            const { 
                nome_filial, 
                id_empresa } = req.body
            
            await knex('filiais').insert({ 
                nome_filial, 
                id_empresa 
            })
            
            return res.status(201).send()
            
        } catch (error) {
            next(error)
        }

    }
}