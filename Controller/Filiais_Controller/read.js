const knex = require('../../banco/connection')

module.exports = {

    async index(req, res, next) {        
       try {
           
        const { id_empresa } = req.params
        const query_alocacao = knex('usuario_alocado')
        const query_empresas = knex('empresas')
        
        var results = {}

            query_empresas
            .where({ id_empresa })

            query_alocacao
            .innerJoin('usuarios', 'usuarios.id_usuario', 'usuario_alocado.id_usuario')
            .innerJoin('filiais', 'filiais.id_filial', 'usuario_alocado.id_filial')
            .where('usuarios.id_empresa', id_empresa)
            .select('usuarios.nome_usuario','filiais.nome_filial')

            const results_empresas = await query_empresas
            const results_usuarios = await query_alocacao

            results = { 'Empresa': results_empresas, 'Afiliados' : results_usuarios }
           
            return res.json(results)
           
       } catch (error) {
           next(error)
       } 
    }

}