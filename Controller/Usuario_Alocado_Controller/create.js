const knex = require('../../banco/connection')

module.exports = {
    async index(req, res, next){ 
        try {
            const { 
                id_usuario, 
                id_filial } = req.body

            await knex('usuario_alocado').insert({
                 id_usuario, 
                 id_filial 
                })
            
            return res.status(201).send()
            
        } catch (error) {
            next(error)
        }

    }
}