module.exports = {
  
  development: {

    client: 'mysql2',
    
    connection: {
      host: '127.0.0.1',
      user:     'root',
      password: '12300',
      database: 'selecao'
    },

    pool: {
      min: 2,
      max: 10
    },

    migrations: {
      tableName: 'migrations_selecao',
      directory:`${__dirname}/banco/migrations`
    },

    seeds: {
      directory: `${__dirname}/banco/seeds`
    }
  }

};
