
exports.seed = function(knex) {
  return knex('filiais').del()

    .then(function () {
  
      return knex('filiais').insert([
  
        { id_empresa: 1, nome_filial: 'Gramil Valparaiso'},
        { id_empresa: 1, nome_filial: 'Gramil Gama'},
        { id_empresa: 2, nome_filial: 'Martelar Santa Maria'},
        { id_empresa: 2, nome_filial: 'Martelar Gama'},
        { id_empresa: 2, nome_filial: 'Martelar Valparaiso'},
        { id_empresa: 3, nome_filial: 'Rede dos Cosmeticos Gama'},
        { id_empresa: 3, nome_filial: 'Rede dos Cosmeticos Valparaiso'},
        { id_empresa: 3, nome_filial: 'Rede dos Cosmeticos Aguas Claras'},
        { id_empresa: 3, nome_filial: 'Rede dos Cosmeticos Santo Antonio'},
        { id_empresa: 3, nome_filial: 'Rede dos Cosmeticos Luziania'},
        { id_empresa: 3, nome_filial: 'Rede dos Cosmeticos Sergipe'}
  
      ]);
    });
};
