
exports.seed = function(knex) {
  return knex('usuario_alocado').del()

    .then(function () {
  
      return knex('usuario_alocado').insert([
  
        {id_filial: 6, id_usuario: 5},
        {id_filial: 2, id_usuario: 1},
        {id_filial: 4, id_usuario: 2},
        {id_filial: 9, id_usuario: 3},
        {id_filial: 1, id_usuario: 4},
  
      ]);
    });
};
