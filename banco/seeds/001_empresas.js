
exports.seed = function(knex) {
  return knex('empresas').del()

    .then(function () {
      
      return knex('empresas').insert([

        { nome_empresa: 'Granmil'},
        { nome_empresa: 'Martelar'},
        { nome_empresa: 'Rede Dos Cosmeticos'}
      
      ]);
    });
};
