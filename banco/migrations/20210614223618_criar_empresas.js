
exports.up = function(knex) {
  
    return knex.schema.createTable('empresas', (table) => {
        table.increments('id_empresa')
        table.string('nome_empresa', 30)
    }) 
  
  
};

exports.down = function(knex) {
    return knex.schema.dropTable('empresas')
  
};
