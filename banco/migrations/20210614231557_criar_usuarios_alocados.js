
exports.up = function(knex) {
  return knex.schema.createTable('usuario_alocado', (table) => {
      table.integer('id_filial')
      table.integer('id_usuario')

      table.foreign('id_filial')
           .references('id_filial')
           
      table.foreign('id_usuario')
           .references('id_usuario')

  })
}

exports.down = function(knex) {
    return knex.schema.dropTable('usuario_alocado')
  
}
