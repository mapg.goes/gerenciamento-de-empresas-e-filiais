exports.up = function(knex) {

    return knex.schema.createTable('usuarios', (table) => {
        table.increments('id_usuario')
        table.string('nome_usuario', 40)
        table.string('email', 50)
        table.string('senha', 18)
        
        table.integer('id_empresa')

        table.foreign('id_empresa')
             .references('id_empresa')
    })
  
};

exports.down = function(knex) {
    return knex.schema.dropTable('usuarios')
  
};
