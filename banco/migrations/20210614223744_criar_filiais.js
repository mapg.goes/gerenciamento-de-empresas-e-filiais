
exports.up = function(knex) {
  
    return knex.schema.createTable('filiais', (table) => {
        table.increments('id_filial')
        table.string('nome_filial', 50)
        
        table.integer('id_empresa')
        
        table.foreign('id_empresa')
             .references('id_empresa')
    
    })

};

exports.down = function(knex) {
    return knex.schema.dropTable('filiais')
  
};
