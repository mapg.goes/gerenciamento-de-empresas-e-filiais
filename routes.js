const express = require('express')


const usuarioCreate = require('./Controller/Usuario_Controller/create')
const usuarioDelete = require('./Controller/Usuario_Controller/delete')
const usuarioAlocadoCreate = require('./Controller/Usuario_Alocado_Controller/create')

const filiaisCreate = require('./Controller/Filiais_Controller/create')
const afiliadosRead = require('./Controller/Filiais_Controller/read')
const filiaisDelete = require('./Controller/Filiais_Controller/delete')

const empresasRead = require('./Controller/Empresas_Controller/read')
const empresasDelete = require('./Controller/Empresas_Controller/delete')
const empresasCreate = require('./Controller/Empresas_Controller/create')


const routes = express.Router()


routes.post('/usuarios', usuarioCreate.index)
      .delete("/usuarios/:id_usuario", usuarioDelete.index)

routes.post('/filiais', filiaisCreate.index)
      .delete("/filiais/:id_filial", filiaisDelete.index)

routes.get('/empresas', empresasRead.index)
      .get('/empresas/afiliados/:id_empresa', afiliadosRead.index)
      .post('/empresas', empresasCreate.index)
      .delete("/empresas/:id_empresa", empresasDelete.index)

routes.post('/alocacao', usuarioAlocadoCreate.index)


module.exports = routes